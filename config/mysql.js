var mysql = require('mysql');

var db_config = {
    host: 'localhost',
    user: 'root',
    password: 'hello123',
    port: 3306,
    database: 'gossip_girl',
    multipleStatements: true
};


    if(process.platform == 'linux'){
        db_config.port = 3306
    }
    if(process.platform == 'darwin'){ //MAC
        db_config.socketPath = '/Applications/MAMP/tmp/mysql/mysql.sock';
    }


function handleDisconnect() {
    connection = mysql.createConnection(db_config); // Recreate the connection, since
    // the old one cannot be reused.

    connection.connect(function (err) {              // The server is either down
        if (err) {                                     // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000); //  introduce a delay before attempting to reconnect,
        }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
    // serving http, display a 503 error.
    connection.on('error', function (err) {
        console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } else {                                      // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}

handleDisconnect();