/**
 * Get login data from local Storage
* */

var loginData= localStorage.getItem("login_data")
loginData=JSON.parse(loginData)
console.log(loginData)

var socket = io.connect(configObj.url);
$(document).ready(function(){


    /**
     * Notify the server your login id
     * */

    socket.emit('logged_in',{user_id:loginData.user_id,user_name:loginData.user_name})


    /**
     * Add data to RIGHT column and selector
    * */

    $('#message_selector').append('<optgroup id="user_group" value="-------USERS--------" label="-------USERS--------"></optgroup>')

    for(var i=0;i<loginData.users.length;i++){
        var checked=''
        if(checkIfUserSubscribed(loginData.users[i].user_name)) {
        checked='checked="checked"'
        }
            $('#users > tbody:last-child').append('<tr><td>' +
                '<label><input type="checkbox" onclick="getRowUser(this)" name="checkbox" style="margin-right: 10px" value="' + loginData.users[i].user_name.trim() + '"'+checked+'>  ' + loginData.users[i].user_name.trim() + '  </label>' +
                '</td>' +
                '</tr>');
            $('#user_group').append('<option value="' + loginData.users[i].user_name + '">' + loginData.users[i].user_name + '</option>')

    }

    /**
     *Add data to LEFT column and selector
    * */

    $('#message_selector').append('<optgroup id="channel_group" value="-------CHANNELS------" label="-------CHANNELS------"></optgroup>')
    for(var j=0;j<loginData.channels.length;j++){
        var checked=''
        if(checkIfChannelSubscribed(loginData.channels[j].topic_name)) {
            checked='checked="checked"'
        }
        $('#channels > tbody:last-child').append('<tr><td>'+
            '<label><input type="checkbox" onclick="getRow(this)" name="checkbox" style="margin-right: 10px" value="'+loginData.channels[j].topic_name.trim()+'"'+checked+'>  '+loginData.channels[j].topic_name.trim()+'  </label>'+
            '</td>'+
            '</tr>');
        $('#channel_group').append('<option value="'+loginData.channels[j].topic_name+'">'+loginData.channels[j].topic_name+'</option>')
    }



    /**
     * Add chanel functionality
    * */

    $('#add_channel_btn').click(function(){
        console.log($('#add_chanel').val())
        if($('#add_chanel').val()!=""){

            socket.emit('add_channel',{"user_name":loginData.user_name,"channel":$('#add_chanel').val(),"user_id":loginData.user_id})
            $('#add_chanel').val("")
        }
    })

    /**
    *  On getting new channel request
    * */

    socket.on('channel_add',function(channelData){
        console.log('channel added',channelData)
        $('#channels > tbody:last-child').append('<tr><td>'+
        '<label><input type="checkbox" onclick="getRow(this)" name="checkbox" style="margin-right: 10px"value="'+channelData.channel.trim()+'">'+channelData.channel.trim()+'  </label>'+
        '</td>'+
        '</tr>');

        addToSelect('#channel_group',channelData.channel)

        //Add to local storage
        loginData.channels.push({topic_name:channelData.channel})
        localStorage.setItem('login_data',JSON.stringify(loginData))
    })

    /**
    *  When a chat message is recieved
    * */

    socket.on('chat_message',function(message){
        $('#main_window > tbody:last-child').append('<tr style="text-align: center"><td>'+message.chat_message+'</td></tr>');
    })

    /**
    * when a chat message from a user
    * is recieved.
    * ONLY CHANGE IN THE ROW STYLE
    * */

    socket.on('chat_message_self',function(message){
        console.log()
        $('#main_window > tbody:last-child').append('<tr style="text-align: right"><td>'+message.chat_message+'</td></tr>');
    })

    /**
     * when a chat message from a user
     * to himself is recieved.
     * ONLY CHANGE IN THE ROW STYLE
     * this will act as a acknowledgement
     * */

    socket.on('chat_message_user',function(message){
        console.log()
        $('#main_window > tbody:last-child').append('<tr style="text-align: left"><td>'+message.chat_message+'</td></tr>');
    })

    /**
    *  when a new user comes online
    * */
    socket.on('new_user',function(userData){
        var checked=''
        if(checkIfUserSubscribed(userData.user_name)) {
            checked='checked="checked"'
        }
        $('#users > tbody:last-child').append('<tr><td>'+
            '<label><input type="checkbox" onclick="getRowUser(this)" name="checkbox" style="margin-right: 10px" value="'+userData.user_name.trim()+'"'+checked+'>  '+userData.user_name.trim()+'  </label>'+
            '</td>'+
            '</tr>');

        addToSelect('#user_group',userData.user_name)

        //Add to local storage
        loginData.users.push({user_name:userData.user_name})
        localStorage.setItem('login_data',JSON.stringify(loginData))
    })

    /**
    * when a user disconnects
    * */
    socket.on('remove_user',function(userName){
        $('#users > tbody  > tr').each(function(n) {
            if($(this).text().trim()==userName.user_name.trim()){
                $(this).closest('tr').empty();
            }
        });

        //Remove from local storage
        //splice the value
        loginData.users.splice({user_name:userName.user_name},1)
        localStorage.setItem('login_data',JSON.stringify(loginData))


        //Remove from select
        $('#user_group option[value="'+userName.user_name+'"]').remove();
    })


    /**
    * post a message
    **/

    $('#send_message').click(function(){
        var selectedReciever=$('#message_selector option:selected').text();
        if(selectedReciever!="Select option to post"){
            if($('#message_input').val()!=""){

                var label = $($('#message_selector')[0].options[$('#message_selector')[0].selectedIndex]).closest('optgroup').prop('label');
                if(label=='-------USERS--------'){
                    console.log()
                    socket.emit('send_to_user',{message:$('#message_input').val(),to:$("#message_selector").val().trim(),user_name:loginData.user_name})
                }else{
                    socket.emit('send_to_channel',{message:$('#message_input').val(),to:$("#message_selector").val().trim(),user_name:loginData.user_name})
                }
            }else{
                alert("Please enter a message")
            }
        }else{
            alert("Select a valid receiver")

        }
    })
})



/**
 * Subscribing and unsubscribing  to a channel
 * */
function getRow(checkbox) {
    if (checkbox.checked)
    {
        socket.emit('subscribe_channel',{"user_name":loginData.user_name,"channel": checkbox.value.trim(),"user_id":loginData.user_id})
        loginData.subscribed_channels.push({topic_name:checkbox.value.trim()})
    }else{
        socket.emit('unsubscribe_channel',{"user_name":loginData.user_name,"channel": checkbox.value.trim(),"user_id":loginData.user_id})
        loginData.subscribed_channels.splice({topic_name:checkbox.value.trim()},1)

    }
    localStorage.setItem('login_data',JSON.stringify(loginData))
}

/**
 * Subscribing and unsubscribing  to a User
 * */
function getRowUser(checkbox) {
    if (checkbox.checked)
    {
        console.log(checkbox.value)
        socket.emit('subscribe_user',{"user_name":loginData.user_name,"user": checkbox.value.trim(),"user_id":loginData.user_id})
        loginData.subscribed_users.push({user_name:checkbox.value.trim()})
    }else{
        socket.emit('unsubscribe_user',{"user_name":loginData.user_name,"user": checkbox.value.trim(),"user_id":loginData.user_id})
        loginData.subscribed_users.splice({user_name:checkbox.value.trim()},1)

    }
    localStorage.setItem('login_data',JSON.stringify(loginData))


}

/**
 * Adding data to the selector
* */
function addToSelect(section,data){
$(section).append('<option value="'+data+'">'+data+'</option>')
}

/**
* Check if the user we are adding to the list
* is already subscribed by this user
* */

function checkIfUserSubscribed(userName){
    for(var i=0;i<loginData.subscribed_users.length;i++){
       if(loginData.subscribed_users[i].user_name==userName){
           return true;
           break;
       }
        if(i==loginData.subscribed_users.length-1){
            return false;
        }
    }
}

/**
 * Check if the channel we are adding to the list
 * is already subscribed by this user
 * */

function checkIfChannelSubscribed(channel){
    for(var i=0;i<loginData.subscribed_channels.length;i++){
        if(loginData.subscribed_channels[i].topic_name==channel){
            return true;
            break;
        }
        if(i==loginData.subscribed_channels.length-1){
            return false;
        }
    }
}