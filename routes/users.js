var express = require('express');
var router = express.Router();

/* POST: Register new user */

router.post('/register', function(req, res, next) {
  var md5=require('md5')
  req.body.password=md5(req.body.password)

  var sqlToInsertUser="INSERT INTO `tb_user` (`user_name`,password) VALUES (?,?)"
  connection.query(sqlToInsertUser,[req.body.user_name,req.body.password],function(err,resultInsert){
    if(err) {
      res.status(400).send("Something is wrong with the data you entered")
    }
    else{
      res.status(200).send("Please login to continue")
    }
  })
});


/*POST: User login */
router.post('/login', function(req, res, next) {

  var md5=require('md5')
  console.log(req.body.password)
  req.body.password=md5(req.body.password)

  var sqlToInsertUser="SELECT `user_id`,`user_name` FROM tb_user WHERE `user_name`=? AND `password`=? LIMIT 1;" +
      "SELECT `user_name` FROM tb_user WHERE `user_name`<>? AND `password`<>? and socket_id<>'';"+
      "SELECT `topic_name` FROM `tb_subscription_topics`;" +
      "SELECT `topic_name` FROM `tb_subscription_topics` LEFT JOIN `tb_subscription_user` ON subscription_id=tb_subscription_topics.u_id " +
      "LEFT JOIN tb_user ON tb_subscription_user.user_id=tb_user.user_id WHERE `user_name`=? AND password=?;" +
      "SELECT `user_name` FROM `tb_user` LEFT JOIN `tb_user_user_map` ON subscribed_to=tb_user.user_id WHERE subscriber_id=(SELECT `user_id`FROM tb_user WHERE user_name=?" +
      " AND password=? )"
  console.log(req.body.user_name,req.body.password);
  connection.query(sqlToInsertUser,[req.body.user_name,req.body.password,req.body.user_name,req.body.password,
    req.body.user_name,req.body.password,req.body.user_name,req.body.password],function(err,resultUser){
    if(err) {
      console.log(err)

    }
    else{
      if(resultUser[0][0]) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
        res.status(200)
        res.send(JSON.stringify({
          "user_id": resultUser[0][0].user_id,
          "user_name": resultUser[0][0].user_name,
          "users": resultUser[1],
          "channels": resultUser[2],
          "subscribed_channels": resultUser[3],
          "subscribed_users": resultUser[4]
        }));
      }else{
        res.status(400)
        res.send("Username and Password incorrect")
      }
    }
  })
});

module.exports = router;
