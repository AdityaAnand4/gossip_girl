io.on('connection', function (socket) {
    console.log("user Connected")

    /**
    *
    *Whenever a disconnect takes place
    * */

    socket.on('disconnect',function(){
        console.log(socket.id)
        var sqlTogetUserId="SELECT `user_id`,`user_name` FROM tb_user WHERE socket_id=? LIMIT 1"
        connection.query(sqlTogetUserId,[socket.id],function(err,resultId){
            if(err){
                console.log(err)
            }else{
                if(resultId.length>0) {
                    console.log('disconnected')
                    console.log(resultId[0].user_id)
                    var sqlToUpdateUser = "UPDATE `tb_user` SET `socket_id`='' WHERE user_id=? LIMIT 1"
                    connection.query(sqlToUpdateUser, [resultId[0].user_id], function (err, resultUpdate) {
                        console.log(resultId[0].user_name)
                        io.sockets.emit('remove_user', {user_name: resultId[0].user_name})
                    })
                }
            }

        })
    })


    /**
     * Whenever a new person has logged in
     * add socket id to db for channel and user broadcasts
     * */

    socket.on('logged_in', function (userId) {
        var sqlToInsertSocketId = "UPDATE `tb_user` SET `socket_id`=? WHERE `user_id`=? LIMIT 1"
        connection.query(sqlToInsertSocketId, [socket.id, userId.user_id], function (err, resultUpdate) {
            if (err) {
                console.log(err)
            }else{
            socket.broadcast.emit('new_user',{user_name:userId.user_name})
            }
        })
    })


    /**
     * Add a channel
     * */
    socket.on('add_channel', function (reqData) {
        console.log('got it')
        var sqlToInsertChannel = "INSERT INTO `tb_subscription_topics` (`user_id`,`topic_name`) VALUES (?,?);"
        connection.query(sqlToInsertChannel, [reqData.user_id, reqData.channel], function (err, resultInsert) {
            if (err) {
                console.log(err);
            } else {
                //Broadcast the channel addition event
                io.sockets.emit("channel_add", {
                    "message": reqData.user_name + " Added channel " + reqData.channel,
                    "channel": reqData.channel
                })
            }
        })
    })



    /**
     * subscribe a channel
     * */

    socket.on('subscribe_channel', function (subscribeChannel) {
        //Insert the data to subscription table
        var sqlToInsertSubscription = "INSERT INTO `tb_subscription_user` (`subscription_id`,`user_id`) VALUES ((SELECT `u_id` FROM `tb_subscription_topics`" +
            " WHERE `topic_name`=? LIMIT 1),?);"
        connection.query(sqlToInsertSubscription, [subscribeChannel.channel, subscribeChannel.user_id], function (err, resultInsert) {
            if (err) {
                console.log(err)
            } else {
                //post the subscription message to every person associated with the channel
                getsubscribedUsers(subscribeChannel.channel,function(err,resultUsers){
                    //Send subscription notification
                    for (var i = 0; i < resultUsers.length; i++) {
                        var message = ""
                        if (subscribeChannel.user_id == resultUsers[i].user_id) {
                            message = "You subscribed to the channel " + subscribeChannel.channel;
                        } else {
                            message = subscribeChannel.user_name + " has subscribed to channel " + subscribeChannel.channel
                        }
                        io.to(resultUsers[i].socket_id).emit('chat_message', {chat_message: message})
                    }
                })

            }
        })

    })


    /**
     * UN-subscribe a channel
     * */

    socket.on('unsubscribe_channel', function (unsubscribeChannel) {
        var sqlToDeleteRelation = "DELETE FROM `tb_subscription_user` WHERE user_id=? AND subscription_id=(SELECT `u_id` FROM `tb_subscription_topics`" +
            " where `topic_name`=? LIMIT 1)LIMIT 1"
        connection.query(sqlToDeleteRelation, [unsubscribeChannel.user_id, unsubscribeChannel.channel], function (err, resultDelete) {
            if (err) {
                console.log(err)
            } else {
                socket.emit('chat_message',{chat_message:"You unsubscribed to the channel " + unsubscribeChannel.channel})
                getsubscribedUsers(unsubscribeChannel.channel, function (err, resultUsers) {
                    //Send subscription notification
                    for (var i = 0; i < resultUsers.length; i++) {
                        var    message = unsubscribeChannel.user_name + " has unsubscribed to channel " + unsubscribeChannel.channel
                        io.to(resultUsers[i].socket_id).emit('chat_message', {chat_message: message})
                    }
                })
            }
        })
    })



    /**
    *  subscribe a user
    * */

    socket.on('subscribe_user',function(subscribeUser){
        var sqlToSubscribeUser="INSERT INTO `tb_user_user_map` (`subscriber_id`,`subscribed_to`) VALUES (?,(SELECT `user_id` FROM `tb_user` WHERE " +
            "`user_name`=? LIMIT 1));";
        connection.query(sqlToSubscribeUser,[subscribeUser.user_id,subscribeUser.user],function(err,resultInsert){
            if(err){
                console.log(err)
            }else{
                var sqlTogetsocketId="SELECT `socket_id` FROM tb_user WHERE user_name=? LIMIT 1;"
                connection.query(sqlTogetsocketId,[subscribeUser.user],function(err,resultSocket){
                    if(err){
                        console.log(err)
                    }else{
                        io.to(resultSocket[0].socket_id).emit('chat_message',{chat_message:subscribeUser.user_name+" has subscribed to you"})
                        socket.emit('chat_message',{chat_message:" you subscribed to "+subscribeUser.user})
                    }
                })
            }
        })
    })


    /**
     *  UN-subsrcibe  a user
     * */

    socket.on('unsubscribe_user',function(unsubscribedUser){
       var sqlToDeleteRelation="DELETE FROM `tb_user_user_map` WHERE `subscriber_id`=? AND `subscribed_to`=(SELECT `user_id` FROM tb_user" +
           " WHERE `user_name`=? LIMIT 1)LIMIT 1;"
        connection.query(sqlToDeleteRelation,[unsubscribedUser.user_id,unsubscribedUser.user],function(err,resultDeletion){
            if(err){
                console.log(err)
            }else{
                console.log(unsubscribedUser.user_id,unsubscribedUser.user)
                console.log(resultDeletion)
            socket.emit('chat_message',{chat_message:'you unsubscribed '+unsubscribedUser.user})
            }
        })
    })



    /**
    * Post message to a user
    * */
    socket.on('send_to_user',function(messageData){
        var sqlToSendMessage="SELECT `socket_id` FROM tb_user WHERE `user_name`=? LIMIT 1;"
        connection.query(sqlToSendMessage,[messageData.to],function(err,resultData){
            if(err){
                console.log(err)
            }else{
                console.log('emitting')
                io.to(resultData[0].socket_id).emit('chat_message_user',{chat_message:messageData.user_name+" --> "+messageData.message})
                socket.emit('chat_message_self',{chat_message:'you - '+messageData.user_name+' --> '+messageData.message})
            }
        })

        //Show the message to subscribers of the reciever
        var sqlToInformSubscribers="SELECT `socket_id`,tb_user.user_name FROM `tb_user` LEFT JOIN `tb_user_user_map` ON tb_user_user_map.subscriber_id=tb_user.user_id" +
            " WHERE `subscribed_to`=(SELECT `user_id` FROM `tb_user` WHERE `user_name`=?)"
        connection.query(sqlToInformSubscribers,[messageData.to],function(err,resultSockets){
            for(var sockets in resultSockets){
                console.log(resultSockets[sockets])
                if(resultSockets[sockets].user_name !=messageData.to && resultSockets[sockets].user_name != messageData.user_name){
                    console.log("emit")
                    io.to(resultSockets[sockets].socket_id).emit('chat_message',{chat_message:messageData.user_name+" posted "+messageData.message+" to "+messageData.to})
                }
            }
        })
    })

    /**
    * sending message to a channel
    * */
    socket.on('send_to_channel',function(messageData) {
    var sqlToGetSubscribedUsers="SELECT `socket_id` FROM `tb_user` LEFT JOIN `tb_subscription_user` ON tb_user.user_id = tb_subscription_user.user_id" +
        " LEFT JOIN tb_subscription_topics ON tb_subscription_topics.u_id= tb_subscription_user.subscription_id WHERE tb_subscription_topics.topic_name=? "
    connection.query(sqlToGetSubscribedUsers,[messageData.to],function(err,resultSockets){
        for(var i=0;i<resultSockets.length;i++){
            io.to(resultSockets[i].socket_id).emit('chat_message',{chat_message:messageData.user_name+" has posted "+messageData.message+" IN--> "+messageData.to})
        }
    })
    })

    })

function getsubscribedUsers(channel,callback) {
    var sqlToGetSubscribedusers = "SELECT `socket_id`,tb_user.`user_id` FROM `tb_user` LEFT JOIN `tb_subscription_user` ON tb_subscription_user.user_id=tb_user.user_id " +
        "LEFT JOIN `tb_subscription_topics` ON tb_subscription_topics.u_id = tb_subscription_user.subscription_id WHERE tb_subscription_topics.topic_name=?"
    connection.query(sqlToGetSubscribedusers, [channel], function (err, resultUsers) {
        if (err) {
            console.log(err)
            return callback(err,null)
        } else {
            return callback(err,resultUsers)
        }
    })

}