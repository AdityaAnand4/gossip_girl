# README #


### What is this repository for? ###

This repository holds the code for a subscription based chat system.
The features are as follows
1. The user registers himself with the app
2. User logs in with the created credentials
Now,The user ca see the list of online users and a list of channels to which he can subscribe, Now this is where the fun kicks in

User gets to know when someone is following him (say a girl) but he will not be notified if he/she has stopped following him/her,

User can also PM someone but the users subscribed to that user (who is sending message) can view the message that he sent.

Any message posted to the group is visible to all the users of that group.

### how to use the app ###
Once logged in, there is a add channel box that can be used to add a channel to the channel to the app

there is also a users tab that contains the information of the users that are logged in,you can subscribe to them by checking the checkbox.

To Post  message there is a selector with options of channels and user, You can select the respective receiver and post a message

### How do I get set up? ###

* The server is Up and running on AWS So to use the app just go to http://52.11.36.215:8001/
* I will also be sending the code and DB backup to you email
* just unzip the code and run "sudo npm install" of course after installing node and npm
* navigate to bin directory and run "sudo node www"
* server will be up then but you need to change the database config
* For that just go to config folder and open mysql.js, You can change the user,host and password there

### Contribution guidelines ###

* No test cases are written yet
* please review the code and let me know how can I make it better